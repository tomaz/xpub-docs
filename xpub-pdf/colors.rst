******
Farben
******

Mit dem Modul ``colors`` werden Farbfunktionen erzeugt. Die Farbfunktionen können mit ``xpub:[name]([tint{0-100}])`` aufgerufen werden und können in der Settingsdatei, in XML-Attributen der Satzdatei und in Processing-Instructions verwendet werden.

Die Farbfunktionen rufen die in FO verfügbare Funktion ``rgb-icc()`` auf und befüllt sie mit den richtigen Werten.

Mögliche Farben
===============

=====================  ================================================================
white                  Standardfunktion für Weiß, Ausnahme: ohne Tint xpub:white()
gray                   Standardfunktion für Schwarz
cmyk                   Prozessfarbe
cmyk, rgb              Prozessfarbe mit RGB für Monitorausgabe
separation, cmyk       Volltonfarbe
separation, cmyk, rgb  Volltonfarbe mit RGB für Monitorausgabe
=====================  ================================================================

Definition
==========

===============  ================================================
Name             colors
Element von      layout
Inhalt           (color)*
Attribute        --
===============  ================================================

.. code-block:: dtd

    <!ELEMENT colors (color)*>
    <!ELEMENT color ((gray | white) | (cmyk, rgb?))?>
    <!ATTLIST color 
        name CDATA #REQUIRED
        separation (true|false) #IMPLIED
        separation-name CDATA #IMPLIED>
    <!ELEMENT gray (#PCDATA)>
    <!ELEMENT white (#PCDATA)>
    <!ELEMENT cmyk (#PCDATA)>
    <!ATTLIST cmyk 
        c CDATA #REQUIRED
        m CDATA #REQUIRED
        y CDATA #REQUIRED
        k CDATA #REQUIRED>
    <!ELEMENT rgb (#PCDATA)>
    <!ATTLIST rgb 
        r CDATA #REQUIRED
        g CDATA #REQUIRED
        b CDATA #REQUIRED>

Beispiel Definition
===================

.. code-block:: xml

    <colors>
        <color name="black">
            <gray/>
        </color>
        <color name="white">
            <white/>
        </color>
        <color name="orange1">
            <cmyk c="0" m="50" y="100" k="0"/>
            <rgb r="243" g="146" b="0"/>
        </color>
        <color name="orange2">
            <cmyk c="0" m="50" y="100" k="0"/>
        </color>
        <color name="orange3" separation="true" separation-name="HKS 20">
            <cmyk c="0" m="50" y="100" k="0"/>
            <rgb r="243" g="146" b="0"/>
        </color>
        <color name="orange4" separation="true" separation-name="HKS 500">
            <cmyk c="0" m="50" y="100" k="0"/>
        </color>
    </colors>


Beispiele Verwendung
====================

In der Settings Datei:

.. code-block:: xml

    <style>
        <setting color="xpub:blue(75)"/>
    </style>

In der Satzdatei Datei:

.. code-block:: xml
    
    <?xpub-format-element-para color="xpub:orange(100)"?>
    <cell background="xpub:orange(10)">
        <para>Lorem ipsum dolor sit amet.</para>
        <para>Lorem ipsum dolor sit amet.</para>
    </cell>