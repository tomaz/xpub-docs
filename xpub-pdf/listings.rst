********
Listings
********

Mit dem Modul ``listings`` können Listen aus Elementen erstellt werden. In der PDF-Ausgabe werden der Text und die Seitenzahlen der Fundstelle ausgegeben.


Definition
==========

===============  ================================================
Name:			 listings
Element von      layout
Inhalt           (listing)*
===============  ================================================

.. code-block:: dtd

	<!ELEMENT listings (listing)*>

===============  ================================================
Name:			 listing
Element von      listings
Inhalt           (title, marker*, page-number, seperator, entry*)
Attribute 		 name CDATA, context (root | current-sequence)
===============  ================================================

.. code-block:: dtd

	<!ELEMENT listing (title, marker*, page-number, separator, entry*)>
    

Beispiel Settings Datei
=======================

.. code-block:: xml

    <listings>	
    	<listing
            name="table-of-contents"
            context="root">
            <title>
                <text>
                    <default>Inhaltsverzeichnis</default>
                    <de>Inhalt</de>
                    <en>Contents</en>
                </text>
                <style>
                    <settings
                        font-family="$headline-fonts"
                        font-size="24pt"
                        line-height="1.2em"
                        space-after="37.5pt"
                        font-weight="bold"/>
                </style>
            </title>
            <marker
                name="col-title-left"
                xpath="'Inhaltsverzeichnis'">
                <style ref="caption"/>
            </marker>
            <marker
                name="col-title-right"
                xpath="'Inhaltsverzeichnis'">
                <style ref="caption"/>
            </marker>
            <page-number>
                <style>
                    <settings
                        font-weight="normal"
                        font-family="$headline-fonts"
                        min-width="0.6cm"
                        text-align="right"
                        start-indent="0pt"
                        end-indent="0pt"
                        text-indent="0pt"/>
                </style>
            </page-number>
            <seperator>
                <style>
                    <settings
                        font-weight="normal"
                        text-align="right"
                        leader-pattern="dots"
                        leader-alignment="reference-area"
                        leader-pattern-width="0.5em"
                        margin-left="1mm"/>
                </style>
                <container-style>
                    <settings
                        min-width="1cm"/>
                </container-style>
            </seperator>
            <entry 
                name="toc-level1"
                xpath="head[@role='chapter'][not(ancestor::section/@role='dedication')]"
                xpath-enumerator="enum/* | enum/text()"
                xpath-text="text/*[not(self::footnote)] | text/text()">
                <style ref="toc-entry">
                    <settings
                        space-before="12.5pt"/>
                </style>
                <before 
                    xpath="following-sibling::*[1][local-name()='author'] | following-sibling::*[2][local-name()='author']">
                    <style>
                        <settings
                            font-family="$headline-fonts"
                            font-weight="normal"
                            font-style="italic"/>
                    </style>    
                </before>
                <after 
                    xpath="preceding-sibling::*[1][local-name()='author'] | preceding-sibling::*[2][local-name()='author']">
                    <style>
                        <settings
                            font-family="$headline-fonts"
                            font-weight="normal"
                            font-style="italic"/>
                    </style>    
                </after>
            </entry>
        </listing>
    </listings>



