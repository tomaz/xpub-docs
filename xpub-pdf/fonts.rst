*********
Schriften
*********

Mit dem Modul ``fonts`` werden Variablen für Schriften erstellt, die mit ``$[name]`` in der Settingsdatei verwendet werden können. 


Definition
==========

===============  ================================================
Element von      layout
Inhalt           (font)*
Attribute        --
===============  ================================================

.. code-block:: dtd

    <!ELEMENT fonts (font)*>
    <!ELEMENT font EMPTY>
    <!ATTLIST font 
        variable CDATA #REQUIRED
        family CDATA #REQUIRED>

Beispiel Definition
===================

.. code-block:: xml

    <fonts>
        <font 
            variable="headline-fonts" 
            family="ScalaSansPro, Lucida Sans Unicode, sans-serif"/>
        <font 
            variable="text-fonts" 
            family="ScalaPro, Times New Roman, serif"/>
        <font 
            variable="table-fonts" 
            family="ScalaSansPro, Lucida Sans Unicode, sans-serif"/>
    </fonts>


Beispiel Verwendung
===================

In der Settings Datei:

.. code-block:: xml

    <style>
        <settings font-family="$headline-fonts"/>
    </style>
