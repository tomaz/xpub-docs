********************
Das XSL/FO-Framework
********************

Mit dem Framework können über eine XML-Settings Datei XSL/FO-Stylesheets definiert werden.


Settings
========

* Layout
  
  - Root
  - Fonts
  - Colors
  - Page-layouts
  - Sequences
  - Listings
  - Footnotes
  - List-Types
  - Styles
  - Blocks
  - Inlines
  - Images
  - Tables
  - Table Styles
  - Equations