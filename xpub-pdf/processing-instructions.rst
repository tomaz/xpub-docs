***********************
Processing-Instructions
***********************

Zugehörig: XSL/FO / Satzanweisungen

Die xpub Processing-Instructions überschreiben in der XML Datei XSL/FO-Attribute aus dem Stylesheet oder erzeugen Anweisungen zur Satzsteuerung. Processing-Instructions werden für das Feintuning im Satz verwendet.


xpub-attribs
============

``<?xpub-attribs [all-attributes]?>``

Formatiert das Elternelement.

.. code-block:: xml
	
	<para>
	<?xpub-attribs 
	  space-after="6pt"
	  color="xpub:blue(100)"
	  background="xpub:orange(10)" ?>Weit hinter den Wortbergen …</para>
	<inline><?xpub-attribs baseline-shift="30%" ?>Weit hinter den Wortbergen …</inline>

Attribute
---------

Je nach dem wo ``<?xpub-attribs?>`` auftritt müssen die passenden XSL/FO-Attribute verwendet werden. Sehen Sie hierzu die Dokumentationen zu `fo:block`_ | `fo:block-container`_ | `fo:inline`_ | `fo:table`_ | `fo:table-cell`_ | `fo:list-block`_ | `fo:list-item`_


xpub-format-blocks
==================

``<?xpub-format-blocks [block-attributes]?>`` … ``<?xpub-format-blocks-end?>``

Formatiert alle folgenden Blockelemente bis ``<?xpub-format-blocks-end?>``.

.. code-block:: xml
	
	<?xpub-format-blocks font-size="9pt" font-style="italic" color="xpub:black(20)"?>
		<para>Lorem ipsum dolor sit amet.</para>
		<para>Lorem ipsum dolor sit amet.</para>
		<list>
			<entry>
				<para>Lorem ipsum dolor sit.</para>
			</entry>
			<entry>
				<para>Distinctio amet similique dolorum.</para>
			</entry>
		</list>
	<?xpub-format-blocks-end?>

Attribute
---------

Attribut Referenz: `fo:block`_


xpub-format-inlines
===================

``<?xpub-format-inlines [inline-attributes]?>`` … ``<?xpub-format-inlines-end?>``

Formatiert alle folgenden Inlineelemente bis ``<?xpub-format-inlines-end?>``.

.. code-block:: xml
	
	<?xpub-format-inlines font-variant="small-caps"?>
		<para>Lorem <author>ipsum dolor<author> sit amet.</para>
		<para>Lorem ipsum <author>dolor sit</author> amet.</para>
	<?xpub-format-inlines-end?>

Attribute
---------

Attribut Referenz: `fo:inline`_


xpub-format-element-[name]
==========================

``<?xpub-format-element-[name] [all-attributes]?>`` … ``<?xpub-format-element-[name]-end?>``

Formatiert alle folgenden Elemente [name] bis ``<?xpub-format-element-[name]-end]?>``.

.. code-block:: xml
	
	<?xpub-format-element-citation start-indent="5mm" end-indent="5mm"?>
		<para>Lorem <author>ipsum dolor<author> sit amet.</para>
		<citation>Lorem <author>ipsum dolor<author> sit amet.</citation>
		<para>Lorem ipsum <author>dolor sit</author> amet.</para>
		<citation>Lorem <author>ipsum dolor<author> sit amet.</citation>
		<para>Lorem ipsum <author>dolor sit</author> amet.</para>
		<citation>Lorem <author>ipsum dolor<author> sit amet.</citation>
		<para>Lorem ipsum <author>dolor sit</author> amet.</para>
	<?xpub-format-element-citation?>

Attribute
---------

Attribut Referenz: alle


xpub-float
==========

``<?xpub-float [float|margin-x|margin-y|padding|width|background]?>``…``<?xpub-float-end?>``

Gefloatete Elemente werden vom Text umflossen.

.. code-block:: xml

	<para>Lorem ipsum dolor sit amet, consectetur adipisicing.</para>
	<?xpub-float 
		float="top right"
		width="40mm"
		margin-x="5mm 0mm"
		margin-y="0mm 5mm"
		padding="2mm"
		background="xpub:black(7)"?>
		<para>Lorem ipsum dolor sit amet, consectetur adipisicing.</para>
	<?xpub-float-end?>
	<para>Lorem ipsum dolor sit amet, consectetur adipisicing.</para>

Attribute
---------

==========  ================================================================================================
Name        Werte [default]
==========  ================================================================================================
float       top, left, right, bottom, start, before, end, after (horizontal, vertical, kombinierbar) [right]
margin-x    in mm, pt, em, rem, px [4mm]
margin-y    in mm, pt, em, rem, px [0.5mm]
padding     in mm, pt, em, rem, px [0mm]
width       in mm, pt, em, rem, px, % [48%]
background  xpub:color, Hex-Code, rgb-icc() [transparent]
==========  ================================================================================================


xpub-rotate
===========

``<?xpub-rotate?>`` … ``<?xpub-rotate-end?>``

Rotation im rechten Winkel.

Attribute
---------

==========  =================
Name        Werte [default]
==========  =================
degree      90, 180, 270, [0]
==========  =================


xpub-page-break, xpub-column-break
==================================

``<?xpub-page-break?> | <?xpbr?>`` : Seitenumbruch

``<?xpub-column-break?> | <?xpcr?>`` : Spaltenumbruch

``<?xpub-line-break?>`` | <?xbr?> : Zeilenumbruch


nh
==

``<?nh?>`` … ``<?nhe?>``

No Hyphenation. Schaltet Silbentrennung und Zeilenumbruch ab.


tab
===

``<?tab?>`` : Tabstop

In Verbindung mit ``@axf:tab-stops`` können ab Version 6.5 im AHF Tabstopps verwendet werden. Die Abstandswerte beziehen sich immer auf den vorhergehenden Tabstop. Wenn ``left 30mm right 70mm`` eingestellt wird, liegt der zweite Tabstop auf 100mm.

.. code-block:: xml

	<para><?xpub-attribs axf:tab-stops="decimal 30mm right 70mm"?>
		Jim Beam <?tab tab-align=","?>17,00€ <?tab?>700ml <br/>
		Absolut  <?tab tab-align=","?>35,00€  <?tab?>700ml
	</para>

Attribute
---------

==========  ============================================
Name        Werte [default]
==========  ============================================
tab-align   Das Zeichen an dem ausgerichtet werden soll.
==========  ============================================


Feste Abstände/Leeräume <?u00A0?>, <?u2004?>
============================================

=========  ===========================================================
PI         Beschreibung
=========  ===========================================================
<?u00A0?>  1/4 Geviert, no breakspace
<?u2004?>  1/3 Geviert, three per em space
<?u2009?>  1/5 Geviert, thin space
<?u200A?>  15/100 Geviert, hair space
=========  ===========================================================


xpub-marker
===========

``<?xpub-marker [mixed]?>`` : Überschreibt den Inhalt des für das Tag definierten Markers. Marker werden für die Ausgabe von Kolumnentiteln oder Registertabs verwendet. In der Regel werden Marker für das Überschriftenelement definiert.

Hinweise: 

	* Überschreibt nur den Text, nicht den Zähler
	* Tags können aktuell 3 Ebenen verschachtelt werden

.. code-block:: xml

	<head level="1">
		<?xpub-marker
		Text der anstelle der <bold>Überschrift</bold> erscheinen soll
		?>
		<enum>1</enum>
		<text>Text der Überschrift</text>
	</head>


xpub-listing-text
=================

``<?xpub-listing-text [mixed]?>`` : Überschreibt den Text des Elementes für die Ausgabe in Listings. Listings werden zum erzeugen von Verzeichnissen (Inhaltsverzeichnis, Abbildungsverzeichnis) verwendet.

Hinweise: 

	* Überschreibt nur den Text, nicht den Zähler
	* Tags können aktuell 3 Ebenen verschachtelt werden

.. code-block:: xml

	<head level="1">
		<?xpub-listing-text
		Text der anstelle der Überschrift im Inhaltsverzeichnis erscheinen soll
		?>
		<enum>1</enum>
		<text>Text der Überschrift</text>
	</head>


xpub-start-pagenr
=================

``<?xpub-start-pagenr [value]?>`` : Stellt die Startseitenzahl der ersten PDF Seite ein. Bei mehrfacher Verwendung wird nur die erste PI berücksichtigt.

.. code-block:: xml

	<book>
		<?xpub-start-pagenr value="5"?>
	</book>


de, es, fr, es
==============

``<?de?>`` … ``<?ede?>``, ``<?fr?>`` … ``<?efr?>`` : Inline Sprache umstellen.

.. code-block:: xml

	<para>
		Als Bart sagte <?en?>"I want an Apple"<?een?>, meinte er keinen Apfel.
	</para>


xpub-label
==========

``<?xpub-label|xl [width]?>`` .. ``<?xpub-label-end|xle?>``: Stellt markierten Text vor den Einzug eines Absatzes an den Anfang des Satzbereichs.

.. code-block:: xml
	
	<?xpub-format-element-para start-indent="5em"?>
	<para>
		<?xl?>Daniel:<?xle?> Lorem ipsum dolor sit amet, consectetur adipisicing.
	</para>
	<para>
		<?xl?>Mark:<?xle?> Lorem ipsum dolor sit amet, consectetur adipisicing.
	</para>
	<para>
		<?xl?>Frodo:<?xle?> Lorem ipsum dolor sit amet, consectetur adipisicing.
	</para>

Attribute
---------

==========  ============================================
Name        Werte [default]
==========  ============================================
width       in mm, pt, em, rem, px [5em]
==========  ============================================


xpub-position
=============

``<?xpub-position|xpos [all-attributes]?>`` … ``<?xpub-position-end?>``: Erzeugt einen Container, der relativ zu seinem Ankerpunkt positioniert und formatiert werden kann.

.. code-block:: xml
	
	<?xpub-position start-indent="0mm" left="-20mm" background="#aaa" padding="1em" font-size="8em" top="0pt" axf:transform="rotate(-23)" color="#bbb" axf:transform-origin="top right" width="15mm"?>AAAAA<?xpub-position-end?>
	<para>Lorem …</para>

Im oberen Beispiel ist der Ankerpunkt links über dem Absatz. Steht ``xpub-position`` innerhalb eines Absatzes ist der Ankerpunkt links neben der Zeile.

Attribute
---------

Alle Block-Attribute.


xpub-line-numbers
=================

``<?xpub-line-numbers[axf:line-number-*]?>`` … ``<?xpub-line-numbers-end?>``: Zeigt Zeilennummern an.

.. code-block:: xml
	
	<?xpub-line-numbers?>
	<para>Lorem ipsum dolor sit amet.</para>
	<para>Lorem ipsum dolor sit amet.</para>
	<para>Lorem ipsum dolor sit amet.</para>
	<?xpub-line-numbers-end?>

	<!-- Zähler wieder bei 1 beginnen. -->
	<?xpub-line-numbers axf:line-number-restart="force"?>
	<para>Lorem ipsum dolor sit amet.</para>
	<para>Lorem ipsum dolor sit amet.</para>
	<para>Lorem ipsum dolor sit amet.</para>
	<?xpub-line-numbers-end?>

Attribute
---------

Antennahouse Formatter axf:line-number-* Attribute.


xpub-leader, xpub-spacer, xpub-force-leader-expansion
=====================================================

``<?xpub-leader|xld [pattern|length|width|leader-attributes]?>``: Element zeichnet eine Linie mit ``fo:leader``.

``<?xpub-spacer|xsp [pattern|length|width|leader-attributes]?>``: Element erzeugt einen Leerraum mit ``fo:leader``.

``<?xpub-force-leader-expansion|xfle?>``: Weist den Elternblock an variable fo:leader auszutreiben.

.. code-block:: xml
	
	<para><?xfle?>Renate: <?xpub-spacer?><?xpub-leader length="110mm" pattern="dots"?></para>
	<para><?xfle?>Tom: <?xpub-spacer?><?xpub-leader length="110mm" pattern="dots"?></para>
	<para><?xfle?>Renate: <?xpub-spacer?><?xpub-leader length="110mm"?></para>
	<para><?xfle?>Tom: <?xpub-spacer?><?xpub-leader length="110mm" ?></para>
	<para><?xfle?>Renate: <?xpub-spacer?><?xpub-leader length="110mm"?></para>

	<para><?xpub-attribs line-height="18pt" text-indent="0pt" space-before="24pt"?>Spätestens zum <?xpub-leader?> alljährlichen <?xpub-leader?> <nh>Weltalphabetisierungstag im September</nh> <?xpub-leader?> wird diese Aussage wieder <?xpub-leader?> und wieder getätigt. Rechnet <?xpub-leader?> man die Zahhlen auf die Erwerbstätigen als Gruppe um, so kann gesagt werden, dass etwa jede_r zehnte Erwerbstätige von funktionalem Analphabetismus betroffen ist. (vgl. ZEIT ONLINE 2017)</para>

Attribute
---------

Attribut Referenz: `fo:leader`_

==========  ====================================================
Name        Werte [default]
==========  ====================================================
width       in mm, pt, em, rem, px [5em] (rule-thickness)
pattern     rule, space, dots (leader-pattern)
length      in mm, pt, em, rem, px [4em] (leader-length.minimum)
==========  ====================================================


Weblinks
========

XSL/FO Dokumentationen (Data2Type)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* fo:block: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-block/>`_
* fo:block-container: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-block-container/>`_
* fo:inline: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-inline/>`_
* fo:table: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-table/>`_
* fo:table-cell: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-table-cell/>`_
* fo:list-block: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-list-block/>`_
* fo:list-item: `<https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-list-item/>`_

.. _fo:block: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-block/

.. _fo:block-container: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-block-container/

.. _fo:inline: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-inline/

.. _fo:table: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-table/

.. _fo:table-cell: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-table-cell/

.. _fo:list-block: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-list-block/

.. _fo:list-item: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-list-item/

.. _fo:leader: https://www.data2type.de/xml-xslt-xslfo/xsl-fo/xslfo-referenz/elemente/fo-leader/