*********
Übersicht
*********

.. image:: images/xpub-xml-icon128.png

.. image:: images/xpub-xml-icon128.png

.. image:: images/xpub-xml-icon128.png

.. image:: images/xpub-xml-icon128.png

.. image:: images/xpub-xml-icon128.png

Module
======

* xpub-dtd
* xpub-word
* xpub-xml
* xpub-pdf
* xpub-epub


xpub-word
=========

Das Addin stellt Formatvorlagen und Funktionen für Formatierung zu Verfügung.


xpub-xml
========

Konvertiert Word nach XML.


xpub-pdf
=======

Definiert und generiert Stylesheets anhand einer Settings Datei und stellt Processing-Instructions für die Formatierung im XML zur Verfügung.


xpub-epub
=========

`in progress …`


Workflow
========

* Datenaufbereitung in Word
* Konvertierung nach XML
* Ausgabe in PDF, `EPUB, Html und weitere Formate`
