*********
xpub-word
*********

**Das Microsoft Word Addin der xpub Publishing Suite**

.. toctree::
   :maxdepth: 2
   :caption: Inhalt:

1 Warum mit xpub-word arbeiten?
===============================

Damit Texte die in Word erfasst werden zuverlässig nach XML konvertiert werden können, ist es unumgänglich die Worddatei richtig zu formatieren. Nur so kann gewährleistet werden, dass jedem Text das richtige Element oder die richtige Bedeutung zugewiesen wird. Das xpub-word Addin bietet Ihnen einen Rahmen um Ihre Daten konsequent zu formatieren. Dafür stellt es Ihnen ein Formatvorlagenset, Formatierungshilfen und -funktionen zur Verfügung.

Dazu gehören:

* **xpub Formatvorlagen**

   - Absatzformate
   - Zeichenformate
   - Tabellenformate

* **xpub Tab im Ribbon:**

   - Formate ersetzen, Format Assistent
   - Schnellzugriff für Formatvorlagen: Überschriften, Absätze, Listen, Zeichen
   - Bausteine für Abbildungen, Tabellen, Textboxen
   - Dokumentfunktionen: Leere Absätze löschen, Hoch-/Querformat, xpub Formate laden

* **Shortcuts für Formatvorlagen**

Die Formatvorlagen dienen der semantischen Auszeichnung und sind vom Stil her einfach gehalten. Das Manuskript soll unabhängig von der späteren Ausgabe formatiert werden.

2 Installation
==============

Kopieren Sie die Datei xpub-word-[version].dotm in den STARTUP Ordner von Microsoft Word.

Passen Sie gegebenenfalls Ihre Makrosicherheitseinstellungen an.

3 Formatvorlagen
================

3.1 Überschriften
-----------------

xpub-word stellt Ihnen verschiedene Überschrifteneben zur Gliederung ihres Textes zur Verfügung:

* Dokumentebene: ``xpub_Dokumenttitel``, ``xpub_Dokumentuntertitel``

* Kapitelebene: ``xpub_Kapiteltitel``, ``xpub_Kapiteluntertitel``

* Gliederungsebene: 
   - Nummeriert: ``xpub_Überschrift_[1-9]``
   - Nicht nummeriert: ``xpub_Überschrift_[1-9]_nn``

* Zwischenüberschriften: ``xpub_Zwischenüberschrift[_fett/_kursiv]``

Anhand Überschriften kann xpub-xml eine Kapitelstruktur erzeugen. Siehe hierzu in der xpub-xml Dokumentation den Abschnitt Kapitelstruktur.

Shortcuts
^^^^^^^^^

+-------------------------------------+----------+
| Name                                | Shortcut |
+=====================================+==========+
| Überschriften                       |          |
+-------------------------------------+----------+
| ``xpub_Überschrift_1``              | Alt+U,1  |
+-------------------------------------+----------+
| ``xpub_Überschrift_2``              | Alt+U,2  |
+-------------------------------------+----------+
| ``xpub_Überschrift_3``              | Alt+U,3  |
+-------------------------------------+----------+
| ``xpub_Überschrift_4``              | Alt+U,4  |
+-------------------------------------+----------+
| Zwischenüberschriften               |          |
+-------------------------------------+----------+
| ``xpub_Zwischenüberschrift``        | Alt+U,5  |
+-------------------------------------+----------+
| ``xpub_Zwischenüberschrift_fett``   | Alt+U,6  |
+-------------------------------------+----------+
| ``xpub_Zwischenüberschrift_kursiv`` | Alt+U,7  |
+-------------------------------------+----------+
| Dokumenttitel                       |          |
+-------------------------------------+----------+
| ``xpub_Dokumenttitel``              | Alt+U,8  |
+-------------------------------------+----------+
| ``xpub_Dokumentuntertitel``         | Alt+U,9  |
+-------------------------------------+----------+

3.2 Absatzformate
-----------------

Fließtext
^^^^^^^^^

``xpub_Standard``

Abstract
^^^^^^^^

Am Anfang eines Textes steht oft eine Zusammenfassung oder eine Einleitung. Diese kann mit den Formaten ``xpub_Abstract_Überschrift_[EN/DE]`` und ``xpub_Abstract_[EN/DE]`` ausgezeichnet werden. Die Formate sind mit Ländercodes versehen und die Spracheinstellungen entsprechend angepasst. So kann sichergestellt werden, dass in der weiteren Verarbeitung die richtige Sprache zum Einsatz kommt. In diesem Muster können weitere Sprachen angelegt werden.

Autoren
^^^^^^^

Autoren eines Beitrags oder Kapitels werden mit ``xpub_Autor`` gekennzeichnet. In der Regel steht der Autor nach der ersten Überschrift, die von ihm stammt.

Beispiel:
"""""""""

Maximilian Mustermann

Keywords
^^^^^^^^

Keywords/Stichworte die den Artikel beschreiben werden mit ``xpub_Keyword`` formatiert. Diese stehen in der Regel nach einer Überschrift.

Beispiel:
"""""""""

[[ BILD ]]

Literatur
^^^^^^^^^

Literaturangaben zeichnen Sie mit ``xpub_Literatur`` aus. Wenn Sie einen Abschnitt „Literatur“ kennzeichnen möchten, benutzen Sie ``xpub_Literatur_Übeschrift``.

Beispiel:
"""""""""

Zabeck, J. (1961). Die realistische Aufgabe der kaufmännischen Berufsschule unter dem Aspekt eines pädagogischen Berufsbegriffes. Die Deutsche Berufs- und Fachschule, 57 (12), 901-916.

Fußnoten
^^^^^^^^

Absätze in Fußnoten werden mit ``xpub_Fußnote`` formatiert. Die Referenz im Text wird nicht gesondert formatiert, sondern behält das von Word automatisch zugewiesene Format.

Beispiel:
"""""""""

Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Böses getan hätte, wurde er eines Morgens verhaftet. „Wie ein Hund!“ sagte er, es war, als sollte die Scham ihn überleben.

Dokument-ID
^^^^^^^^^^^

Sie können am Anfang des Dokumentes einen Absatz mit ``xpub_Dokument_ID`` formatieren. Dies kann hilfreich sein um in der späteren Konvertierung eindeutige IDs und Bildnamen zu erzeugen.

Beispiel:
"""""""""

xdocs0001

Tipp
""""

Daraus können IDs wie xdocs0001-h1.1 oder Bildnamen wie xdocs0001-abb0001.tif erzeugt werden. Das kann hilfreich sein, wenn Teile von außerhalb des Dokumentes referenziert werden sollen.

Shortcuts
^^^^^^^^^

+-----------------------+--------------+
| Name                  | Shortcut     |
+=======================+==============+
| ``xpub_Standard``     | Strg+Shift+1 |
+-----------------------+--------------+
| ``xpub_Fußnote_Text`` | Strg+Shift+2 |
+-----------------------+--------------+

3.3 Listenformate
-----------------

Das Addin bietet Ihnen vier Listentypen mit drei Ebenen:

Für geordnete Listen: ``xpub_Liste_abc`` und ``xpub_Liste_123``

Für ungeordnete Listen: ``xpub_Liste_Strich`` und ``xpub_Liste_Punkt``

Das Addin kann um weitere Listenformate erweitert werden. Damit xpub-xml die Listenformate korrekt verarbeiten kann, gehen Sie beim Anlegen weiterer Listenformate wie folgt vor:

1) Legen Sie für die Listenebenen die Sie brauchen Absatzformate an. Bennen Sie Zum Beispiel:

   * ``xpub_Liste_XIV_Ebene1``
   * ``xpub_Liste_XIV_Ebene2``

2) Legen Sie eine neue Formatvorlage vom Typ Liste an. Vergeben Sie den gleichen Namen (``xpub_Liste_XIV``) wie bei den Absatzformaten, nur ohne _Ebene_[n]. Weisen Sie unter Format > Nummerierung den Listenebenen die vorher definierten Absatzformate und die Art des Aufzählungszeichens zu. 

Shortcuts
^^^^^^^^^

+-------------------------------------------+----------+
| Name                                      | Shortcut |
+===========================================+==========+
| Nummerische Liste                         |          |
+-------------------------------------------+----------+
| ``xpub_Liste_123_Ebene1``                 | Strg+1,1 |
+-------------------------------------------+----------+
| ``xpub_Liste_123_Ebene2``                 | Strg+1,2 |
+-------------------------------------------+----------+
| ``xpub_Liste_123_Ebene3``                 | Strg+1,3 |
+-------------------------------------------+----------+
| Alpha Nummerische Liste (Kleinbuchstaben) |          |
+-------------------------------------------+----------+
| ``xpub_Liste_abc_Ebene1``                 | Strg+A,1 |
+-------------------------------------------+----------+
| ``xpub_Liste_abc_Ebene2``                 | Strg+A,2 |
+-------------------------------------------+----------+
| ``xpub_Liste_abc_Ebene3``                 | Strg+A,3 |
+-------------------------------------------+----------+
| Aufzählung mit Punkten                    |          |
+-------------------------------------------+----------+
| ``xpub_Liste_Punkt_Ebene1``               | Strg+P,1 |
+-------------------------------------------+----------+
| ``xpub_Liste_Punkt_Ebene2``               | Strg+P,2 |
+-------------------------------------------+----------+
| ``xpub_Liste_Punkt_Ebene3``               | Strg+P,3 |
+-------------------------------------------+----------+
| Aufzählung mit Strichen                   |          |
+-------------------------------------------+----------+
| ``xpub_Liste_Strich_Ebene1``              | Strg+D,1 |
+-------------------------------------------+----------+
| ``xpub_Liste_Strich_Ebene2``              | Strg+D,2 |
+-------------------------------------------+----------+
| ``xpub_Liste_Strich_Ebene3``              | Strg+D,3 |
+-------------------------------------------+----------+

3.4 Formate für Abbildungen und Tabellen
----------------------------------------

Abbildungen und Tabellen benötigen oft zusätzliche Angaben wie eine Beschriftung, eine Quelle oder eine Legende. Hierfür stehen jeweils folgende Formate zur Verfügung:

**Abbildungen**

* Beschriftung/Caption: ``xpub_Abb_Überschrift``

* Anmerkung/Legende: ``xpub_Abb_Anmerkung``

* Quellenangabe: ``xpub_Abb_Quelle``

* Alternativtext: ``xpub_Abb_Alt_Text``

* Copyright: ``xpub_Abb_Copyright``

* Verarbeitungsanweisungen: ``xpub_Abb_Platzierung``

**Tabellen**

* Beschriftung/Caption: ``xpub_Tab_Überschrift``

* Anmerkung/Legende: ``xpub_Tab_Anmerkung``

* Quellenangabe: ``xpub_Tab_Quelle``

* Alternativtext: ``xpub_Tab_Alt_Text``

* Copyright: ``xpub_Tab_Copyright``

* Tabellenfußnoten: ``xpub_Tab_Fußnote``

Wie Tabellen und Abbildungen richtig eingefügt werden, lesen Sie in den Kapiteln „Tabellen“ und „Abbildungen“.

3.5 Formate für Textboxen/Kästen
--------------------------------

Das xpub-word Addin verzichtet bewusst auf den Einsatz von Textrahmen um Textboxen und Kästen umzusetzen. Textrahmen sind aufgrund Ihrer vielfältigen Eigenschaften und Platzierungsmöglichkeiten mit Blick auf eine XML-Konvertierung potenzielle Fehlerquellen. Stattdessen wird mit Start- und Endformaten gearbeitet. Dies bringt mehr Sicherheit in der Konvertierung.

Um diese Start- und Endformate in ein Element für eine Textbox/einen Kasten zu konvertieren benutzen Sie in xpub-xml einen Start-End-Wrapper.

Das Addin stellt ein Start-Ende-Paar für eine Textbox bereit. 

* ``xpub_Textbox_Start``

* ``xpub_Textbox_Ende``

Wenn Sie weitere Kastentypen benötigen, können Sie die bestehenden kopieren und umbenennen. Zum Beispiel in ``xpub_meine_Box_Start`` und ``xpub_meine_Box_Ende``. Zur besseren Unterscheidung können Sie eine andere Hintergrundfarbe wählen.

4 Der xpub Tab im Ribbon
========================

Der xpub Tab stellt Ihnen einige Formatierungshilfen und Automatisierungstools zur Verfügung, mit denen Sie das Dokument für die XML Konvertierung vorbereiten können.

.. figure:: images/xpub-tab.png

  Abb. 1: Der xpub Tab in Microsoft Word 2013

4.1 Formate zuweisen
--------------------

Mit dem Makro „Formate ersetzen“ können Sie das Dokument nach Formaten durchsuchen, die nicht mit dem Präfix xpub beginnen. Das Makro zeigt ein Fenster in dem die gefunden Formate aufgelistet werden. Hier können Sie das passende xpub Format zuweisen. Das Makro versucht anhand der Formatnamen automatisch eine Zuordnung zu finden.

.. figure:: images/formate-zuweisen.png

  Abb. 2: Das „Formate zuweisen“ Fenster

4.2 Format-Assistent
--------------------

Mit dem Format-Assistenten können Sie das Dokument Schritt für Schritt formatieren. Mit den einfachen Pfeilen „<“ und „>“ springen Sie zum nächsten Absatz. Mit den Doppelpfeilen „<<“ und „>>“ springen Sie zum nächsten Format, das nicht mit xpub beginnt. Oben wird der Name des aktuellen Formats angezeigt. Sie können das neue Format oben aus der Dropdown-Liste wählen oder die Buttons in den Reitern „Häufig verwendet“ und „Abbildungen/Tabellen“ verwenden.

.. figure:: images/format-assistent.png

   Abb. 3: Der Format-Assistent

4.3 Überschriften, Absätze, Listen, Zeichen
-------------------------------------------

Damit Sie die wichtigsten Absatzformate schnell parat haben, finden Sie diese auch im xpub Tab.

.. figure:: images/xpub-tab-formate.png

   Abb. 4: Schnellzugriff für häufige verwendete Formate im xpub Tab

4.4 Abbildungen
---------------

Der Abschnitt „Abbildungen“ bietet Ihnen zwei Möglichkeiten einen Abbildungsblock einzufügen. Diese dienen nicht dem Einfügen der Abbildung selbst, sondern dem korrekten Einfügen der Zusatzinformationen zu einer Abbildung. Also der Überschrift, der Anmerkung, der Quelle usw. Ob die Abbildung tatsächlich im Worddokument eingefügt wird, kommt auf Ihren Workflow an. 

4.4.1 Vorlage einfügen
^^^^^^^^^^^^^^^^^^^^^^

Der Button „Vorlage einfügen“ öffnet eine Dialogbox in der Sie auswählen können welche Bestandteile eingefügt werden sollen. Außerdem wird ein Zähler eingefügt, auf den Sie über „Verweise > Querverweis > Verweistyp: Abb.“ verweisen können.

.. figure:: images/abbildung-vorlage.png

   Abb. 5: Vorlage Einfügen: Abbildung

4.4.2 Text formatieren
^^^^^^^^^^^^^^^^^^^^^^

Mit dem Button „Text formatieren“ können Sie einem bestehenden Text xpub Formate zuweisen. Dafür gibt es einige Schlüsselwörter anhand das Makro erkennt, welches Format zugewiesen werden soll. Allerdings wird hier kein echter Zähler als referenzierbarer Verweistyp eingefügt.

Tipp
""""

Schlüsselwörter sind: Abb., Abbildung, Image, Figure, Fig., Anmerkung, Annotation, Legend, Quelle, Source, Alt-Text

Beispiel:
"""""""""

| Abb. 16: Das ist eine Abbildung
| Anmerkung: Die Abbildung ist toll.
| Copyright: Beim Hersteller
| Quelle: Aus der Luft gegriffen.

Wird zu:

.. image:: images/text-formatieren.png



4.5 Tabellen
------------

Die Makros „Vorlage einfügen“ und „Text formatieren“ funktionieren genau wie die bei „Abbildungen“. „Text formatieren“ sucht allerdings nach Tab., Tabelle und Table.

4.5.1 Tabelle formatieren
^^^^^^^^^^^^^^^^^^^^^^^^^

Mit „Tab. formatieren“ öffnen Sie die zugehörige Dialogbox. Hier können Sie für die Tabelle Einstellungen vornehmen, die von xpub-xml in ein benutzerdefiniertes XML konvertiert werden können. Sehen Sie hierzu die Einstellungen im Abschnitt Tabellen in der xpub-xml Dokumentation.

.. figure:: images/tabelle-formatieren.png

   Abb. 6: Tabelle formatieren Dialog

Hintergrund
^^^^^^^^^^^

Im Bereich „Hintergrund“ können Sie per Klick auf ein Feld Ihre Tabelle einfärben. In xpub-xml können die Grauwerte dann einer Farbe zugewiesen werden.

Navigieren
^^^^^^^^^^

Mit „Navigieren“ springen Sie zur nächsten oder vorherigen Tabelle.

Rahmen
^^^^^^

Um Teile einer Tabelle besser kenntlich zu machen oder deutlicher abzugrenzen, sind unterschiedliche Linientypen hilfreich. Unter „Rahmen“ können Sie unterschiedliche Strichstärken und Linientypen wählen oder Linien ausblenden. Diese Einstellungen können von xpub-xml konvertiert werden.

Tabellenformate
^^^^^^^^^^^^^^^

Das Addin bringt 4 Tabellenformate mit, die Sie über die Buttons 1 bis 4 zuweisen können. Einstellungen von Tabellenformaten können mit „Hintergrund“ und „Rahmen“ überschrieben werden.

Verschiedenes
^^^^^^^^^^^^^

Dezimal Tabstop: Markieren Sie eine Spalte mit kommaseparierten Zahlen aus und klicken Sie auf „Dezimal Tabstop“ um die Zahlen mittig am Komma auszurichten. Diese Einstellung wird von xpub-xml übernommen.

Spalten ausgleichen: Markieren Sie die Spalten die gleich breit werde sollen und klicken Sie auf „Spalten ausgleichen“.

4.6 Textboxen
-------------

Vorlage einfügen
^^^^^^^^^^^^^^^^

Fügt die Start- und Endformate ein und positioniert den Cursor in einem leeren Absatz dazwischen.

Marker setzen
^^^^^^^^^^^^^

Markieren Sie den Bereich den Sie in eine Textbox setzen möchten und klicken auf „Marker setzen“. Das Makro fügt vor und hinter der Markierung die Start- und Endformate für eine Textbox ein.

4.7 Dokument
------------

Formate laden
^^^^^^^^^^^^^

Bevor Sie beginnen ein Dokument zu formatieren, müssen Sie mit „Formate laden“ die xpub Formatvorlagen in das Dokument laden. Dies ist auch Voraussetzung für alle Funktionen die Formatvorlagen benötigen.

Leere Absätze löschen
^^^^^^^^^^^^^^^^^^^^^

Zu Beginn oder am Ende der Formatierung sollten für die weitere Verarbeitung alle leeren Absätze gelöscht werden. Vermeiden Sie Abstände durch leere Absätze zu erzeugen. Bei der XML Ausgabe sind diese in der Regel nicht gewünscht. Sollten Sie tatsächlich einen leeren Absatz für die XML Ausgabe brauchen, fügen Sie ein geschütztes Leerzeichen (Strg+Shift+Leertaste) in einen leeren Absatz ein.

Hoch-/Querformat
^^^^^^^^^^^^^^^^

Breite Tabellen können im Querformat besser gestaltet werden. Für die PDF Ausgabe fügt xpub-xml eine Processinginstruction zum Steuern der Seitenorientierung in der XML Ausgabe ein. 

5 Ein Dokument formatieren
==========================

Installieren Sie das xpub-word Addin wie im Abschnitt Installation beschrieben.

5.1 Einen neuen Text formatieren
--------------------------------

Öffnen Sie ein neues Dokument und laden Sie die xpub Formatvorlagen über „Dokument > Formate laden“ im xpub Tab.

Wenn Sie einen neuen Text erstellen und diesen gleich von Anfang an mit dem xpub-word Addin formatieren möchten, nutzen Sie am besten den xpub Tab und die Shortcuts. Wenn Sie die Formate geladen haben, können Sie im Dokument die Shortcuts an Ihre Bedürfnisse anpassen. Nutzen Sie die Funktionen für das Einfügen von Tabellen, Abbildungen und Textboxen. 

5.2 Einen bestehenden Text formatieren
--------------------------------------

Wenn Sie einen bestehenden Text formatieren möchten, gehen Sie zum Beispiel so vor:

1) Kopieren Sie die original Datei und öffnen Sie sie in Word

2) Laden Sie die Formatvorlagen über den xpub Tab: Dokument > Formate laden

3) Schauen Sie das Dokument durch und machen Sie sich ein Bild davon wie es formatiert ist. 

Im besten Fall ist die Datei sauber mit Überschriften und Formaten für z.B. Zitate, Abbildungs- und Tabellenüberschriften usw. ausgezeichnet. Dann können Sie die Funktion „Formate ersetzen“ verwenden. Überlegen Sie bei welchen Formaten eine automatische Ersetzung sinnvoll ist und weisen Sie die neuen Formate zu. Formate die nicht ersetzt werden sollen stehen auf „nicht zugewiesen“.

4) Um an Stellen zu springen die noch nicht mit xpub Formaten ausgezeichnet sind, verwenden Sie den Formatassistent. Mit den Buttons „<<“ und „>>“ springen Sie zum nächsten Format, dass nicht mit xpub beginnt.

5) Formatieren Sie Tabellen- und Abbildungselemente wie Beschriftung und Quelle mit den vorgesehenen Formatvorlagen. Nutzen Sie ggf. „Text formatieren“

6 Übernahme von Wordfunktionen
==============================

6.1 Indexeinträge
-----------------

Word Indexeinträge können übernommen und in xpub-xml deren Ausgabeform definiert werden.

6.2 Verweise
------------

Wenn Sie die das xpub-word Addin zusammen mit xpub-xml verwenden, können Sie folgende Verweistypen verwenden:

* REF
* PAGEREF
* NOTEREF
* HYPERLINK

Folgende Schalter können verwendet werden:

* \\n
* \\r
* \\w
* \\p

Welche Funktion die Schalter im Zusammenspiel mit einem Verweistyp haben, können Sie auf der Microsoft Seite  nachlesen. 


6.3 Formel
----------

Word-Formeln werden nach MathML 2.0 konvertiert.

