********
xpub-xml
********

.. image:: images/xpub-xml-icon128.png

.. toctree::
   :maxdepth: 1
   :caption: Inhalt

   xpub-xml/einfuehrung
   xpub-xml/installation
   xpub-xml/einstellungen
   xpub-xml/custom-xsl
   xpub-xml/typografie
