*************
Einstellungen
*************

DTD
===

.. code-block:: xml

    <!DOCTYPE xpub-xml PUBLIC "-//Rennert Mediendienstleistungen/DTD xpub-xml V 1.0//EN" 
                              "http://xmlsatz.de/xpub/dtd/xpub-xml.dtd">

Projektname
===========

.. code-block:: xml

    <xpub-xml>
        <name>xpub</name>
        …
    </xpub-xml>

Root Element
============

.. code-block:: xml

    <xpub-xml>
        …
        <root>xpub</root>
        …
    </xpub-xml>

Formatvorlagen Elemente zuweisen
================================

.. code-block:: xml

    <elements>
        <element>
            <style>xpubAbstractberschriftDE</style>
            <name>sub-head</name>
            <attribute-set>
                <attribute>
                    <name>role</name>
                    <value>abstract</value>
                </attribute>
            </attribute-set>
        </element>
        …
    </elements>

Nummerierte Überschriften
=========================

.. code-block:: xml

    <numbered-headlines>
        <list-style-id>xpubUeberschriften</list-style-id>
        <style>xpubberschrift</style>        
        <structure>
            <name>head</name>
            <enumerator>enum</enumerator>
            <text>text</text>
            <level-attribute>level</level-attribute>
        </structure>
    </numbered-headlines>

Zeichenformatierungen
=====================

.. code-block:: xml

    <inline-elements>
        <bold>bold</bold>
        <italic>italic</italic>
        <superscript>sup</superscript>
        <subscript>sub</subscript>
        <small-caps>small-caps</small-caps>
        <underline>underline</underline>
        <strike>strike</strike>
        <language>language</language>
    </inline-elements>

Listen
======

.. code-block:: xml

    <lists>
        <list-prefix>xpubListe</list-prefix>
        <list>
            <name>list</name>
            <entry-name>entry</entry-name>
            <style>xpubListe123</style>
            <list-style>arabic</list-style>
        </list>
        …
    </lists>

Tabellen
========

.. code-block:: xml

    <tables>
        
        <table-reference>
            <name>table-ref</name>
        </table-reference>
        <table>
            <name>table</name>
            <attribute-set>
                <attribute>
                    <name>xpub-wrapper-class</name>
                    <value>table</value>
                </attribute>
            </attribute-set>
        </table>
        <table-columns-wrapper>
            <name>cols</name>
        </table-columns-wrapper>
        <table-columns>
            <wrapper>
                <name>cols</name>
            </wrapper>
            <name>col</name>
            <attribute-set>
                <width>width</width>
                <column-name>num</column-name>
            </attribute-set>
        </table-columns>
        <table-header>
            <name>thead</name>
        </table-header>
        <table-footer>
            <name>tfoot</name>
        </table-footer>
        <table-body>
            <name>tbody</name>
        </table-body>
        <table-row>
            <name>row</name>
        </table-row>
        <table-cell>
            <name>cell</name>
            <attribute-set>
                <number-rows-spanned>rowspan</number-rows-spanned>
                <number-columns-span>colspan</number-columns-span>
            </attribute-set>
        </table-cell>
    </tables>

Fußnoten
========

.. code-block:: xml

    <footnotes>
        <name>footnote</name>
    </footnotes>

Verweise
========

.. code-block:: xml

    <cross-references>
        <name>cross-ref</name>
        <attr-ref-id>ref-id</attr-ref-id>
        <attr-ref-type>ref-type</attr-ref-type>
    </cross-references>

Links
=====

.. code-block:: xml

    <hyperlinks>
        <name>link</name>
        <attr-url>url</attr-url>
    </hyperlinks>

Indexeinträge
=============

.. code-block:: xml

    <index-entries> <!-- elements/attributes -->
        <name>index-entry</name>
        <attr-word>word</attr-word>
        <attr-subword>subword</attr-subword>
        <attr-subsubword>subsubword</attr-subsubword>
    </index-entries>

Sonderzeichen
=============

.. code-block:: xml

    <special-characters>
        <symbol>
            <name>symbol</name>
            <font>font</font>
            <char>char</char>
        </symbol>
        <softhyphen remain-softhyphens="false" as="processing-instruction">
            <!-- Werden nicht erhalten -->
            <!-- as processing-instruction | code | element -->
            <name>u00AD</name>
        </softhyphen>
    </special-characters>

Typografie
==========

.. code-block:: xml

    <typography>
        <character-type>pi</character-type>
        <exclude>
            <name>link</name>
            <name>url</name>
            <name>document-id</name>
            <name>xpub-pi</name>
        </exclude>
    </typography>

Bilderexport
============

.. code-block:: xml

    <image-export>
        <output-path>images</output-path>
        <colorspace>RGB</colorspace> <!-- RGB|CMYK|L -->
        <max-width>122</max-width> <!-- mm -->
        <dpi>300</dpi>
    </image-export>

Class Wrapper
=============

.. code-block:: xml

    <class-wrappers>
        <class-wrapper>
            <name>image</name>
            <restart>caption</restart>
            <xpub-wrapper-class>figure</xpub-wrapper-class>
        </class-wrapper>
    </class-wrappers>

    …

    <element>
        <style>xpubLiteratur</style>
        <name>reference</name>
        <attribute-set>
            <attribute>
                <name>xpub-wrapper-class</name>
                <value>literature</value>
            </attribute>
        </attribute-set>
    </element>

Start-End-Wrapper
=================

.. code-block:: xml

    <start-end-wrappers>
        <start-end-wrapper>
            <name>note</name>
            <start>textbox-start</start>
            <end>textbox-end</end>
        </start-end-wrapper>
    </start-end-wrappers>

