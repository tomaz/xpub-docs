**********
Einführung
**********

Was kann xpub-xml?
==================

- Konvertiert Docx-Dateien nach XML
- Konvertiert alle Docx-Dateien in einem Ordner
- Um Daten aus mehreren Docx-Dateien in einem Hauptdokument zusammenzuführen, können diese in einem ZIP-Archiv organisiert und zusammen konvertiert werden.
  
.. image:: ../images/xpub-xml-screen-1.png


Wofür ist xpub-xml gedacht?
===========================

Manuskriptdaten werden häufig in Word erfasst. Wenn die Daten in einen automatisierten Prozess einfließen oder als medienneutrales Format gespeichert werden sollen, werden sie meist als lesbares XML-Format benötigt. Mit ``xpub-xml`` können Sie Docx-Dateien in ein benutzerdefiniertes XML-Format konvertieren. 


Wie funktioniert xpub-xml?
==========================

``xpub-xml`` wird für ein Formatvorlagenset eingestellt. Dafür können Sie ``xpub-word`` oder eine eigene verwenden. In einer Einstellungsdatei (XML) wird festgelegt welche Formatvorlage welchem Element zugewiesen werden soll. Außerdem gibt es Einstellungsmöglichkeiten für nummerierte Überschriften, eine Kapitelstruktur, Listen, Tabellen, Fußnoten, Verweise und Funktionen um Elemente zu schachteln bzw. zu gruppieren. Als letzten Konvertierungsschritt bietet ``xpub-xml`` ein anpassbares XSLT-Stylesheet um eine benutzer- oder projektspezifische Konvertierung anzuhängen.

      
Welche Features bietet xpub-xml?
================================

- Benutzeroberfläche und Kommandozeilentool
- Plattformübergreifend einsetzbar
- Formatvorlagen können Elementen zugewiesen werden
- Zeichenformatierungen können Elementen zugewiesen werden
- XML-Struktur für nummerierte Überschriften
- Verschachtelte Listen
- Tabellen
- Übernahme von Formatierungen wie Tabellenrahmen und Tabellenstilattributen
- Erzeugt auf Wunsch eine Kapitelstruktur
- Mechanismen zum gruppieren von Elementen
- Word Symbole können für das Ausgabeformat definiert werden
- Softhyphens können erhalten oder automatisch entfernt werden
- Kommentare können als Processing-Instruction übernommen werden
- Übernahme von Querverweisen (nicht alle Typen) und Indexeinträgen
- Einfügen von typografischen Festabständen als PI oder Hex-Code
- Elemente wie ``<url>, <isbn>, <art-nr>`` können bei der typografischen Ersetzung ausgeschlossen werden
- Anpassbares Custom-XSL-Stylesheets als letzter Konvertierungsschritt
