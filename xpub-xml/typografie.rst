**********
Typografie
**********

xpub-xml berücksichtigt die wichtigsten mikrotypografischen Richtlinien und fügt unterschiedliche feste Leeräume als Processing-Instruction oder im Hex-Code ein. Im folgenden sehen Sie in vereinfachter Schreibweise die Suchmuster die aktuell implementiert sind.


Datum
-----

**Beispiele:** 01. Januar 2014, 1. Januar 2014      

**Zeichen:** ``u00A0`` (1/4 Geviert)                                                


2-stellige Abkürzungen
----------------------

**Beispiele:** u.a., u. a., u.°a.; z.B., z. B.,     

**Zeichen:** ``u2009`` (1/5 Geviert)                                                


3-stellige Abkürzungen
----------------------

**Beispiele:** i.d.R., i. d. R., i. d.°R., i.°d.°R. 

**Zeichen:** ``u2009`` (1/5 Geviert)                                                


Wort mit Zahl dahinter
----------------------

**Beispiele:** Tabelle 1; Abbildung 1               

**Zeichen:** ``u00A0`` (1/4 Geviert)     

**Liste:** Absatz, Satz, Seite, Heft, Jahrgang, Kapitel, Auflage, Teil, Nummer, Ausgabe, Artikel, Abbildung, Abbildungen, Tabelle, Tabellen, Anlage, Anlagen


Abkz. mit Zahl dahinter
-----------------------

**Beispiele:** Tab. 1, Fig. 1, Abb. 1               

**Zeichen:** ``u200A`` (0.15 Geviert)   

**Liste:** §, €, $, Abs., S., Jg., Kap., Aufl., Jahrg., Ausg., Art., Abb., Tab.                     


Zahl mit Einheit dahinter
-------------------------

**Beispiele:** 100g, 20mm, 5kg                      

**Zeichen:** ``u200A`` (0.15 Geviert)   

**Liste:** %, Ah\., pt, bar, cm, g, h, kg, kJ, m, mg, mm, mol, Mol, ppm, qm, t, f., ff., V, Euro, €, Dollar, $, Pfund, Prozent             


Gesetze
-------

**Beispiele:** SGB I, BGBl. X

**Zeichen:** ``u2009`` (1/5 Geviert)    

**Liste:** SGB, BGBl.


Mathematische Operatoren
------------------------

**Beispiele:** 1+2, n=3, Hund = Katze

**Zeichen:** ``u2009`` (1/5 Geviert)    

**Liste:** +, =, − (Mathematisches Minuszeichen), ∓, ≈, <, >, ≤, ≠


Zahlen mit Bis-Strich
---------------------

**Beispiele:** 2013 - 2015, 2013-2015

**Zeichen:** &#x2013; (Bis-Strich) ohne Leerraum






