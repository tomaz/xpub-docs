Installation
============

Laden Sie sich das passende Archiv herunter:

Mac
---
  
https://xmlsatz.de/xpub/downloads/xpub-xml-mac.dmg

Kopieren Sie den Inhalt des Archives an den gewünschten Ort.
  
Windows
-------

https://xmlsatz.de/xpub/downloads/xpub-xml-win.zip

Kopieren Sie den Inhalt des Archives an den gewünschten Ort.