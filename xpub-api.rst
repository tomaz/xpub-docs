********
xpub-api
********

Voraussetzungen
===============

* Sie brauchen einen Benutzer für die xpub-suite.
* Die Domain von der aus Sie Ihre Requests ausführen, muss in unseren CORS Einstellungen als vertrauenswürdig hinterlegt werden.

API URL
=======

.. code-block:: html

    https://xmlsatz.de/xpub-suite/api/v1

Authentifizierung
=================

Sie brauchen einen Benutzer und ein Token um die API nutzen zu können.

Das Token können Sie über folgende URL abfragen:

.. code-block:: html

    https://xmlsatz.de/xpub-suite/api/v1/auth/login/

Um das Token abzufragen senden Sie einen ``POST``-Request mit Ihrem Benutzernamen und ihrem Passwort:

.. code-block:: js

    {
        'username': 'snoopy',
        'password': '12345'
    }

Als Ergebnis erhalten Sie Ihr Token im data Objekt. Das Token senden Sie bei 
allen weiteren Requests im Http-Header im Bereich "Authorization" mit.

Axios Beispiel:

.. code-block:: js

    axios.defaults.headers.common['Authorization'] = 'token 123456789';

``Wenn Sie das Token nicht selbst abfragen möchten, können sie es auch bei uns anfordern.``

Benutzer
========

Um Informationen zu Ihrem Benutzerkonto abzufragen senden Sie einen ``GET``-Request an:

.. code-block:: html

    https://xmlsatz.de/xpub-suite/api/v1/user/

Ergebnis
--------

.. code-block:: js

    {
        "first_name": "Snoopy",
        "last_name": "Peanut",
        "username": "snoopy",
        "email": "snoopy@xmlsatz.de",
        "groups": [
            {
                "name": "The Peanuts",
                "workflows": [
                    {
                        "id": 1,
                        "name": "Workflow 1",
                        "description": "",
                        "output_formats": [
                            "pdf_web",
                            "pdf_print"
                        ]
                    },
                    {
                        "id": 2,
                        "name": "Workflow 2",
                        "description": "",
                        "output_formats": [
                            "pdf_web",
                        ]
                    }
                ]
            }
        ]
    }

Jobs
====

Sie können über die API

* Neue Jobs erstellen.
* Einen bestimmten Job abfragen.
* Eine Liste aller Jobs die dem Benutzer gehören abfragen.

Einen neuen Job erstellen
-------------------------

.. code-block:: html

    https://xmlsatz.de/xpub-suite/api/v1/jobs/

Senden Sie einen ``POST``-Request mit dem Content-Type ``multipart/form-data`` und folgendem Inhalt:

* ``name``: Name oder Titel des Jobs (optional, default: Neuer Job)
* ``workflow``: ID des Workflows
* ``docx_file``: Microsoft Word Dokument (docx)
* ``formats``: Array/Liste mit den Formaten die Sie rendern möchten. Standardformate sind: ``pdf_web``, ``pdf_print``

Wenn Sie einen neuen Job erstellt haben erhalten Sie sofort eine Rückmeldung mit der ID des Jobs. 
Die Felder für die Download-URLs und Fehlermeldungen sind zunächst leer.

.. code-block:: js

    {
        "id": 82,
        "name": "Neuer Job",
        "creator": "snoopy",
        "created": "15.02.21/14:31",
        "updated": "15.02.21/14:34",
        "workflow": {
            "id": 1,
            "name": "Workflow 1",
            "description": "",
            "output_formats": [
                "pdf_web",
                "pdf_print"
            ]
        },
        "pages": 0,
        "pages_rendered": 0,
        "docx_file": "/media/xpub_suite/jobs/3091/neuer-job.docx",
        "docx_file_url": "/xpub-suite/download/job/3091/docx",
        "xml_file_url": "",
        "pdf_file_web_url": "",
        "pdf_file_print_url": "",
        "pdf_file_pdfua_url": "",
        "pdf_file_pdfa_url": "",
        "epub_file_url": "",
        "production_stage": "docx",
        "archived": false,
        "thumbnails": {
            "thumb_180": "",
            "thumb_360": ""
        },
        "errors": false,
        "xml_file_error": null,
        "pdf_file_web_error": null,
        "pdf_file_print_error": null,
        "pdf_file_pdfua_error": null,
        "pdf_file_pdfa_error": null
    }

Nun können Sie anhand der ID Ihren Job abrufen und prüfen ob ihre Download-URLs oder Fehlermeldungen vorhanden sind.

Einen Job abfragen
------------------

.. code-block:: html

    https://xmlsatz.de/xpub-suite/api/v1/jobs/[JOB-ID]

Senden Sie einen ``GET``-Request wobei Sie ``[JOB-ID]`` durch Ihre Job-ID erstetzen.

.. code-block:: js

    {
        "id": 3091,
        "name": "Neuer Job",
        "creator": "snoopy",
        "created": "02.05.22/14:20",
        "updated": "02.05.22/14:20",
        "workflow": {
            "id": 1,
            "name": "Workflow 1",
            "description": "",
            "output_formats": [
                "pdf_web",
                "pdf_print"
            ]
        },
        "pages": 10,
        "pages_rendered": 30,
        "docx_file": "/media/xpub_suite/jobs/3091/neuer-job.docx",
        "docx_file_url": "/xpub-suite/download/job/3091/docx",
        "xml_file_url": "/xpub-suite/download/job/3091/xml",
        "pdf_file_web_url": "/xpub-suite/download/job/3091/pdf_web",
        "pdf_file_print_url": "/xpub-suite/download/job/3091/pdf_print",
        "pdf_file_pdfua_url": "/xpub-suite/download/job/3091/pdf_ua",
        "pdf_file_pdfa_url": "",
        "epub_file_url": "/xpub-suite/dummy/epub_2",
        "production_stage": "docx",
        "archived": false,
        "thumbnails": {
            "thumb_180": THUMBNAIL_URL,
            "thumb_360": THUMBNAIL_URL
        },
        "errors": true,
        "xml_file_error": null,
        "pdf_file_web_error": null,
        "pdf_file_print_error": null,
        "pdf_file_pdfua_error": null,
        "pdf_file_pdfa_error": "ERROR MESSAGE"
    }

Liste aller Jobs des Benutzers abfragen
---------------------------------------

.. code-block:: html

    https://xmlsatz.de/xpub-suite/api/v1/jobs/

Senden Sie einen ``GET``-Request und Sie erhalten einen Array/eine Liste mit allen Jobs die dem Benutzer gehören.