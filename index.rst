xpub • XML Publishing Suite
============================

.. toctree::
   :maxdepth: 1
   :caption: Inhalt:

   introduction
   xpub-xml
   xpub-word
   xpub-pdf
   xpub-api

